module.exports = {
  moduleDirectories: ['node_modules', 'src'],
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  testMatch: ['**/*.spec.ts?(x)'],
  transform: {
    '^.+\\.tsx?$': 'babel-jest',
  },
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.ts',
    '\\.(css|less)$': '<rootDir>/__mocks__/fileMock.ts',
  },
  globals: {
    __VERSION__: true,
    __DEV__: true,
    __PRODUCTION__: false,
    __TEST__: true,
  },
  // setupFilesAfterEnv: ['<rootDir>/src/setupTests.ts'],
};
