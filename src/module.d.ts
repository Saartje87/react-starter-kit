// TODO Should / how can we publish this to npm?

declare const __DEV__: boolean;
declare const __PRODUCTION__: boolean;

interface EmConfig {
  backendHost: string;
  settings: any;
  version: string;
}

interface Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: Function;
  emconfig: EmConfig;
}
