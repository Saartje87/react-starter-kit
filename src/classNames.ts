type Arg = null | undefined | boolean | string;

export const classNames = (...args: Arg[]) =>
  args.filter(arg => arg && typeof arg === 'string').join(' ');
