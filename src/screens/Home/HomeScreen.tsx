import React from 'react';
import { hot } from 'react-hot-loader/root';

const HomeScreen = () => {
  return <div className="HomeScreen">HOME</div>;
};

export default hot(HomeScreen);
