import React from 'react';
import { hot } from 'react-hot-loader/root';
import HomeScreen from 'screens/Home';

const RootScreen = () => <HomeScreen />;

export default hot(RootScreen);
