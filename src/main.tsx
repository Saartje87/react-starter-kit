import React from 'react';
import ReactDOM from 'react-dom';
import { setConfig } from 'react-hot-loader';

import AppShell from 'components/AppShell';
import HomeScreen from 'screens/Home/HomeScreen';

setConfig({
  ignoreSFC: true,
  pureRender: true,
});

const mountNode = document.getElementById('root');

ReactDOM.render(
  <AppShell>
    <HomeScreen />
  </AppShell>,
  mountNode,
);

// Scroll to active input whenever viewport changes size
// This is most useful on mobile devices, input is not (always) visible after opening the virtual keyboard.
window.addEventListener('resize', () => {
  if (document.activeElement && document.activeElement.nodeName === 'INPUT') {
    document.activeElement.scrollIntoView({ block: 'center' });
  }
});
