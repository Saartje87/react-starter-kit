import React, { StrictMode } from 'react';
import { Router } from '@blockle/router';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';

import { configureStore } from 'config/store';
import { PersistGate } from 'redux-persist/integration/react';

const { store, persistor } = configureStore();

type Props = {
  children: React.ReactNode;
};

const AppShell = ({ children }: Props) => {
  return (
    <StrictMode>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router history={createBrowserHistory()}>{children}</Router>
        </PersistGate>
      </Provider>
    </StrictMode>
  );
};

export default AppShell;
