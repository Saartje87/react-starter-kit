import { TestReducer } from 'reducers/test';
export { testReducer as test } from 'reducers/test';

export type AppState = {
  test: TestReducer;
};
