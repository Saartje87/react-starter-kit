import { Reducer, AnyAction } from 'redux';

export interface TestReducer {
  count: number;
}

const initialState = {
  count: 0,
};

export const testReducer: Reducer<TestReducer, AnyAction> = (state = initialState, actions) => {
  return state;
};
