const webpack = require('webpack');

const { config, PATHS } = require('./webpack-base.config');

module.exports = {
  ...config,
  devtool: 'eval-source-map',
  devServer: {
    contentBase: PATHS.public,
    disableHostCheck: true,
    hot: true,
    noInfo: false,
    host: '0.0.0.0',
    port: 8000,
  },
  module: {
    ...config.module,
    rules: [
      ...config.module.rules,
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader?sourceMap', 'postcss-loader'],
      },
      {
        test: /\.jsx?$/,
        include: /node_modules/,
        use: ['react-hot-loader/webpack'],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('dev'),
      },
      __DEV__: JSON.stringify(true),
      __PRODUCTION__: JSON.stringify(false),
    }),
    ...config.plugins,
  ],
};
