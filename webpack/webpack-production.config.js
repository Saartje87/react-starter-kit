const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlCriticalPlugin = require('html-critical-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');
// const PurgecssPlugin = require('purgecss-webpack-plugin');
// const glob = require('glob-all');

const { config, PATHS } = require('./webpack-base.config');

const { TARGET_ENV } = process.env;

module.exports = {
  ...config,
  optimization: {
    ...config.optimization,
    minimizer: [
      new OptimizeCSSAssetsPlugin({}),
      new TerserPlugin({
        parallel: true,
        sourceMap: true,
      }),
    ],
  },
  module: {
    ...config.module,
    rules: [
      ...config.module.rules,
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader'],
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      chunkFilename: '[id].[hash].css',
      allChunks: true,
    }),
    // new PurgecssPlugin({
    //   paths: glob.sync([
    //     `${PATHS.src}/**/*`,
    //     `${PATHS.node_modules}/@blockle/ui/dist/**/*`,
    //   ], { nodir: true }),
    // }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      },
      __DEV__: JSON.stringify(TARGET_ENV !== 'production'),
      __PRODUCTION__: JSON.stringify(TARGET_ENV === 'production'),
    }),
    ...config.plugins,
    // Invalid css output? Error "Error: undefined:1:7: missing '}'" from css/lib/parse/index.js:62:15
    new HtmlCriticalPlugin({
      base: PATHS.dist,
      src: 'index.html',
      dest: 'index.html',
      inline: true,
      minify: true,
      extract: true,
      width: 375,
      height: 565,
      penthouse: {
        blockJSRequests: false,
      },
    }),
  ].filter(plugin => !!plugin),
};
