/* eslint-disable @typescript-eslint/camelcase */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const workboxPlugin = require('workbox-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const pkg = require('../package.json');

const PATHS = {
  base: path.resolve(__dirname, '../'),
  dist: path.resolve(__dirname, '../dist'),
  node_modules: path.resolve(__dirname, '../node_modules'),
  public: path.resolve(__dirname, '../src/assets'),
  src: path.resolve(__dirname, '../src'),
};

module.exports.PATHS = PATHS;

module.exports.config = {
  devtool: 'source-map',
  context: PATHS.src,
  entry: {
    app: './main',
  },
  resolve: {
    modules: ['node_modules', PATHS.src],
    extensions: ['.js', '.ts', '.tsx'],
    symlinks: false,
    alias: {
      react: path.resolve('./node_modules/react'),
      'react-dom': path.resolve('./node_modules/react-dom'),
      // react: 'preact/compat',
      // 'react-dom': 'preact/compat',
    },
  },
  output: {
    path: PATHS.dist,
    publicPath: '/',
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader',
      },
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        // include: [PATHS.src, /node_modules\/(@blockle)/],
      },
      // Static image loader
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
        options: {
          // inline base64 URLs for <=xk images
          limit: 2000,
        },
      },
      // Static `svg` image loader
      {
        test: /images(\\|\/).*?\.svg$/,
        loader: 'url-loader',
        options: {
          // inline base64 URLs for <=xk images
          limit: 2000,
        },
      },
      // Transform svg files (in /icons/ folder) to react components.
      {
        test: /icons(\\|\/).*?\.svg$/,
        loader: 'react-svg-loader',
      },
    ],
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      debug: true,
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(PATHS.public, './index.ejs'),
      filename: 'index.html',
      hash: false,
      sentryEnabled: process.env.NODE_ENV === 'production',
    }),
    new webpack.DefinePlugin({
      __VERSION__: JSON.stringify(`v${pkg.version}`),
    }),
    new FaviconsWebpackPlugin(path.resolve(PATHS.public, './images/blockle-logo.png')),
    new WebpackPwaManifest({
      name: 'Blockle UI',
      short_name: 'Blockle UI',
      description: 'Blockle UI for React',
      background_color: '#fff',
      theme_color: '#4da1ff',
      icons: [
        {
          src: path.resolve(PATHS.public, './images/blockle-logo.png'),
          sizes: [96, 128, 192, 256, 384, 512],
        },
      ],
    }),
    // new workboxPlugin.InjectManifest({
    //   swSrc: './src/sw.js',
    //   swDest: 'sw.js',
    //   exclude: [/\.(jpg|png|map|cache)$/],
    // }),
  ],
};
