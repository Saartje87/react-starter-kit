# React Starter Kit

## Install

```bash
yarn
```

## Run dev server with hot reloading

```bash
yarn start
```
